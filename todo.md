[readme](README.md)

## Theo's Projects

### In progress

_Actions wanted_

* tbd
* 2020-04-16 ~ Theo ~ Submit C19 as NPM



### Future efforts / To be discussed

COVID-19


* initial camera position from browser GPS position (+ animation?)
* different visualization: https://codepen.io/Flamov/pen/MozgXb?editors=0010

General Three.js

_Preparing "Cookbook" examples_

* Splash screen pop=up ```div``` in Three.js is an issue
	* Specifying and improving the entire UE and underlying code
	* Probably want
	* Acts as a splash screen
	* Acts as a notes, help text, dialog box display display
	* Acts as a pop-up display when you interact with an in-world object
	* Draggable and resizable
	* Content is selectable, clickable and scrollable
* Getting frames per second back up to 60
	* Easy ways of identifying what is burning up the cycles


### Stats.js

Bookmark to obtain frames per second

<a href="javascript:(function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='https://raw.githack.com/mrdoob/stats.js/master/build/stats.min.js';document.head.appendChild(script);})()" >stat.js</a>

Other

* Create Three.js "inworld" background for video chats


Sharing with Harald

	* Figure out how difficult a PR would be, for interacting with the exctruded countries of https://vasturiano.github.io/three-globe/example/country-polygons/
	* improve Map of http://foodcoops.at ??

### Already improved or in progess, but not completely done

_Actions if there are matters of interest_

* (done) Get Eslint working on Theo's machine and VS Code
    * (fixed) not available for auto-format
    * Review and refine Theo's workflow
    * Add Rules
    * Add Shortcuts?
* Write Covid-19-Viz deployment checklist << in progress. Done maybe??
* Automate Covid-19 Viz deployment << in progress. Done maybe??
* Fix relative link issues << ditto
* Improving 3D Globe visualization



## Harald's Projects

1. Submit 2 small bug-fixes or improvements to Inkscape
    - tba
1. Figure out who developer do (or can do) cpu and memory usage profiling (on different platforms?) and improve the developer documentation for Blender
    - Issue I am working on: https://developer.blender.org/T70016
    - Building Blender: https://wiki.blender.org/wiki/Building_Blender
1. Foodsoft project: https://github.com/foodcoops/foodsoft/pull/716
    - Demo Server: https://foodcoops.github.io/demo/

Design Collaboration Projects

- Visualize locations of Remote-Pair-Programming Partners on the globe with three.js
    - more general: Visualize locations/connections of people on a Globe
    - [x] visualize globe
    - [ ] visualize location (pin?)
    - [ ] visualize a 3D curve, between 2 location
    - [ ] add location via address or from phone address book
    - [ ] klick on globe to add location
    - [ ] show more information on mouse-over/klick (picture, street view, text?)
    - [ ]

