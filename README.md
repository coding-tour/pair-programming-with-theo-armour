[to do]( todo.md )


# pair-programming-with-theo-armour

This repo will be used for discussing and prioritizing collaboration topics.



## 2020-05-24

### Theo

#### https://codesandbox.io/

I once again played with https://codesandbox.io/. I get it running and am able to access a GitHub repo. The issue is that I seem to get lost in the process of synchronizing the data between CodeSandBox and GitHub. In essence, the environment asks for a lot more "full-stack developer" skills than I currently have.

#### https://online.visualstudio.com/

I just tried again. Previous time was a fail. I'm in and I have carried out synch. We may be on to something. Perhaps, the large monopolies like to make things easy for the unwashed masses. ;-)


#### Linting etc

Glitch uses Prettier to clean up JavaScript and CSS anf HTML. Using ESLint to do all three gets complicated. 

I am thinking: Give up Mr.doob style and go with Prettier. I may not liker the way it looks, but single click to fix all three - and Markdown - is worth it. Adding .prettierrc helps.




## 2020-05-22

### Theo


Latest globes are here:

* https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-template/
	* Base file for online interactive 3D Globe designed to be forked, hacked and remixed with real-time interactive 3D graphics in your browser using the WebGL and the Three.js JavaScript library
* https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-population-cities
	* A stress test fpr the globe code
	* How well can it handle data for 15,494 cities in 3D 
* https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-pandemic-worldometer/
	* The basis for the next update of the COVID-19 tracker
	* Eleven data points available for each country: How best to display these in 3D?

In our last meetup Harald noted that that the render calls per frame still numbered in the hundreds. It turned out these were due to the GeoJSON lines. These is now being merged at load time and the the number of calls is much reduced.

***

I am currently playing with four architectural data viewers:

* https://www.ladybug.tools/honeybee-viewer/viewer-lt/
* https://www.ladybug.tools/spider-gbxml-tools/spider-gbxml-viewer/dev/
* https://hoarelea.github.io/sam-viewer/sam-viewer/
* https://www.ladybug.tools/spider-rad-viewer/rad-viewer/r8/

Either this will drive me crazy. Or I will simplify the process so it's easy.

***

Nice video of one of my projects

* https://www.linkedin.com/feed/update/urn:li:activity:6668339642653380609/


***

ESLint, Glitch, deployment and dev.to: all going slowly. Have new computer [Asus Zebook Duo Pro]( https://www.asus.com/us/Laptops/ZenBook-Pro-Duo-UX581GV/ )


*** 

In touch with these people: https://cityzenith.com/

Working buildings using Unreal. 

Did you see the Unreal 5 announcement? It's very unreal. ;-)


## Follow-up from 2020-04-30
## 2020-05-07

* Prettier config (.prettierrc for Glitch): https://prettier.io/docs/en/configuration.html#basic-configuration
    * YAML
``` yaml
# .prettierrc
trailingComma: "es5"
tabWidth: 4
semi: false
singleQuote: true
```

* Use Eslint to run Prettier
    * https://prettier.io/docs/en/integrating-with-linters.html#use-eslint-to-run-prettier
    * NPM command (instead of yarn): `npm install eslint-plugin-prettier ``

* `--save-dev` adds packages to `devDependencies` in package.json which are not shipped to customer

* Mozilla Hubs: https://hubs.mozilla.com
* https://twitter.com/utopiah/
* https://glitch.com/edit/#!/hubs-issues?path=github-issues.js

### Items for discussion

#### ESLint

* Installing ESLint for all the GitHub repos
    * We should do this on my computer   
* Linting for CSS and HTML?


```

	"[html]": {
		"editor.defaultFormatter": "vscode.html-language-features"
	},

	"[css]": {
		"editor.defaultFormatter": "esbenp.prettier-vscode"
	},
```


Are these commands all the same?

* ./node_modules/.bin/eslint
* npm run eslint
* npx eslint
* Global install
* CLI ESLint


ESLint Installs: Global or not & why?

* Recommends not to install globallyL https://eslint.org/docs/user-guide/getting-started
* Recommends? How to install globally: https://eslint.org/docs/user-guide/command-line-interface




#### Discusion items

For Coding on Glitch

* Style guides to follow??
* Cookbook style or lots of little js files?
* Js in init or in main.js?


Items for discussion such as the above are affect the way I develop in many impactful ways. Discussion on topics such as these would help a lot. Perhaps these are similar to W3C request for comments (RFC).

### Theo Accomplishments

Content

* Honeybee Viewer: https://www.ladybug.tools/honeybee-viewer/viewer-lt/v-2020-05-06/
* https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-template/globe-basic-2020-05-06.html
* https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/get-geo-data-global/v-2020-05-06/get-geo-data-regional/get-geo-data-regional.html
* https://support.glitch.com/t/interactive-3d-globe-template-built-with-three-js-update-2020-05-06/23267

Tools

* Researching aspects of ESLint - see above
* Looked into VS Code Cloud

Pair Programming

* Joined https://lunchclub.ai/
* First meetup on Monday: https://www.carijyao.com/







## Followup from 2020-05-04

- how to check the eslint rules? => run eslint from the terminal
    - `npx eslint .` 
    - `npm run lint`

## 2020-05-04


### Theo

Items for discussion

Content
* Stable & dev c19-viz3d are broken?
* VS Code settings file is empty
	* Must remember to "save all" manually
* Thinking about Glitch
* These notes: how to indicate something done? Show all last weeks objectives were met!
* Template strings in ESLint need monitoring


#### Accomplishments

Content

* Dev.to post
	* https://dev.to/theoarmour/a-day-living-in-the-singularity-1l29
* Glitch
	* c19-v3d
		* https://glitch.com/edit/#!/theo-2020-05-02
	* Honeybee Viewer
		* https://honeybee-viewer-lt-2020-05-03.glitch.me/

Tools

* VS Code


Issues / Observations

* Tiny VS Code progress


## Follow-up from 2020-04-30

* It is possible to push branches to Glitch (not master)
    * In the Glitch Terminal window (command line inside the Glitch container) the branch can be merged to master
        * `git merge mybranch`
        * `refresh` (so that editor updates)  
    * https://support.glitch.com/t/possible-to-code-locally-and-push-to-glitch-with-git/2704/3


## 2020-04-30

* Link to Glitch Prettier description (Config): https://support.glitch.com/t/make-your-code-prettier/14768
* https://glitch.com/~tilted-cubes
* https://glitch.com/teams#create-team
	* Theo: Should we create a team? If so, suggest a name.
* Investigate Groups: https://threejs.org/docs/#api/en/core/BufferGeometry.groups

### Theo

Session objectives

* Discuss use of Glitch & Blog posts
	* https://dev.to/glitch/automating-my-deploys-from-github-to-glitch-2fpd
* Examine the instanceMesh tilting issue
	* https://glitch.com/edit/#!/theo-2020-04-29-tilted-bars?path=index.html%3A1%3A0

Accomplishments

* Two Dev.to blog posts
* Progress with using Glitch
* Good thought on moving forward with a globe-making template
* c19-viz3d speed-ups


Issues / Observations

* Nor ESLint progress
* Glint Prettifier is not my choice - so it goes
	* But does prettify combined html, css and JavaScript



Objectives

* Tidy up and enhance previous blog posts
* Work on tilt issues
* add text function to globe template
* clean-up and standardize globe template 
* Add more speed-ups to c19-viz3d


## Follow-up from 2020-04-23

## 2020-04-27

* Link to Gitlch Remix (scaling experiments) of Harald: https://glitch.com/edit/#!/join/4bd0e32d-5fd4-47d6-aac8-f9f23d63727a


### Theo

Accomplishments

* Important c19v3d release
* Two dev.to posts
* Good investigation into speed up
* Re-start Glitch uses

Issues / Observations

* ESLint use is broken in my VSCode
* JS Console Performance tab: cannot find a good use (see below)
* Cal-ver deploy: untouched since last week
* Glitch && Dev.to && Markdown && GitHub could be wonderful


Objectives

* Explore effective cal-ver deployment on Glitch
	* Dev.to as not good way of collaborating on Markdown
	* Glitch Markdown collaboration looks good
	* Solution: draft on Glitch and post on Dev.to - or wherever





#### 2020-04-27


Dev.to Articles

* https://dev.to/theoarmour/c19-viz3d-real-time-pandemic-data-in-3d-17p9
* https://dev.to/theoarmour/2020-04-26-dev-to-glitch-markdown-three-js-3b2g

More Globe links in read me. Scroll down to "Links of Interest"

* https://github.com/ladybug-tools/spider-covid-19-viz-3d/


* Work on globe speed-up

* Slow: https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-bars-many/v-2020-04-25/globe-bars-many.html
* Faster: https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/globe-bars-many/v-2020-04-26/globe-bars-many.html
* Globe template: https://www.ladybug.tools/spider-covid-19-viz-3d/assets/0-templates/globe-basic-2020-04-26.html

Glitch project

* https://glitch.com/edit/#!/ta-2020-04-26


#### 2020-04-24 ~ ESLint template strings

* https://eslint.org/docs/rules/prefer-template
* https://eslint.org/docs/rules/quotes#top
* https://www.ladybug.tools/spider-covid-19-viz-3d/assets/0-templates/globe-basic-2020-04-26.html


Three.js

* https://github.com/mrdoob/three.js/issues/15506
* Example of usage: https://github.com/mrdoob/three.js/blob/dev/src/renderers/shaders/ShaderChunk/encodings_pars_fragment.glsl.js
* Not three.js example: https://discoverthreejs.com/book/introduction/javascript-tutorial-1/


So would following be a good example of the rule?

```
	const  a = 23;
	const htm = `
<h1>Hi there</h1>
<p>
	abc<bc>
	def<bc>
	123<bc>
</p>
`;
	let zz;
```

BTW, formerly I used single quotes everywhere, but now with back ticks I switched to double quotes. They take an extra keystroke but cause fewer type than mixing 'single' and `back tick`;



#### Cal-ver-demo

Is there an easy way to copy or compare files between branches locally?



#### Three.js Performance

I forgot to mention that after the 2020-04-20 session I spent an hour or more playing with the Chrome Developer Console Performance panel. The graphics are very pretty, but they represent such low level processes that the signals are of little use to a high-level coder such as myself. The thing that interests me is whether a particular module or even a specific loop is taking time. I can see no way of seeing this with the Performance panel. A somewhat satisfactory replacement is using console.time or performance.now() inside a routine.

The only useful use for the Performance tab was for ascertaining load times. I spent quite a bit of time seeing whether altering the load order of different modules made any difference in the load time.

I found that changing the placement order of the modules in either the main HML file or main.js created no discernable difference in the load time. This kind of made sense given that JavaScript is asynchronous and will try to everything as fast as it can.

#### Three.js Performance Stats

One that still works

* http://spite.github.io/rstats/
	* http://spite.github.io/rstats/demo.html

## Follow-up from 2020-04-20

### Harald

- Finally read the 4 rules of Improv, and coincidentally heard the "Yes And" concept on the new episode of the Legacy Code Rocks podcast: https://www.legacycode.rocks/podcast-1/episode/270edc0e/practical-empathy-with-indi-young
- Added pictures and conclusion section: https://dev.to/harald3dcv/three-js-visualizations-with-theo-armour-week-2-1fme-temp-slug-7763569?preview=92da7f2c5aee9ae8fcd1d17dce7efeefc5e05924703ab208ee048d621e81dcab969761ed1300b334f90b3054bf0fb089d72dde244d9d4fe497050013


### Theo

* https://www.ladybug.tools/spider-covid-19-viz-3d/dev/ v-2020-04-21 is up
* Shown as part of my new Zoom background
* Looked into screencasting: screencastufy, Nimbus and Loom. Played woth S and L
	* Screencastify wins because goes directly to YouTube and can create GIFs
* Would like to look into
	* Issues with template strings
		* ESLint seem to offer no support
		* To do: See how Mr.doob handles them
	* Where to put inits: main.js or index.html?
	* Formatting () ? xx : xx;
* Might look into c19v3d template files
* Continue exploration of deployment workflow



## Follow-up from 2020-04-20

### Things to look into

* Using Nvidia GPU > Nvidia Control Panel > Chrome enabled << se below
* Set .mergeBuffereGeomtery toGroup = true



### Nvidia Control Panel

![]( images/nvidia-control-panel.png )

* 3D Settings > Manage 3D Settings > Program Settings > Google Chrome > 2. Select preferred graphics processor ...  > High-Performance NVIDIA processor

You will need to close all browser instances a and restart

Control-Shift-escape to bring up task manager


Three.js Performance trackers

* https://github.com/munrocket/gl-bench << seems to be working
	* https://discourse.threejs.org/t/webgl-performance-monitor-with-gpu-loads/9970/10
* http://zz85.github.io/zz85-bookmarklets/threelabs.html << no longer working in recent releases
* https://github.com/jeromeetienne/threejs-inspector << ditto
* http://benvanik.github.io/WebGL-Inspector/ << ditto?
* https://callumprentice.github.io/apps/amui/index.html << now an editor more than a tracker


* https://www.telerik.com/blogs/performance-tune-a-javascript-website-with-chrome-devtools
* https://developers.google.com/web/tools/chrome-devtools/rendering-tools


Pages of interest

* https://codeburst.io/improve-your-threejs-performances-with-buffergeometryutils-8f97c072c14b
* https://www.realtimerendering.com/webgl.html << many links to resources
* https://discourse.threejs.org/t/what-is-a-reliable-way-to-benchmark-a-device/13246
* https://stackoverflow.com/questions/14002195/using-javascript-to-detect-device-cpu-gpu-performance
* https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/finish
* https://evilmartians.com/chronicles/faster-webgl-three-js-3d-graphics-with-offscreencanvas-and-web-workers
* https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_best_practices << very low level

WebGL Stats

* http://webglstats.com/
* http://www.geeks3d.com/webgl/#caps
* https://alteredqualia.com/tmp/webgl-maxparams-test/
* https://github.com/takahirox/WebGL-CPU-Profiler-Extension

### interesting globe visualization libs

Add: - interesting globe visualization libs


### Performance Appraisals

* https://en.wikipedia.org/wiki/Performance_appraisal



### Follow-up from 2020-04-16

#### Theo

* Zoom: installed and tested
* ESLint: no further experiments yet
* Three.js performance
	* C19
		* Now using 4096x2048 image
			* Speed up load maybe but 1 fps better if that
			* matrixUutoUpdate: Added to Globe. Elsewhere: because JavaScript is Asynch. It often initiates too soon. Could creat a module that does the housekeeping later in the load process
			* Combining geometry. When bars are combined into one mesh they are no longer easily accessible for ray=casting and launching pop-ups
C19 latest
	* https://www.ladybug.tools/spider-covid-19-viz-3d/dev/v-2020-04-19/covid-19-remix/c19-remix.html

#### C19 performance improvements
- Found the Chrome Spector.js extension: https://spector.babylonjs.com/ (for analyzing/profiling webgl)
- Globe texture needs 100MB GPU memory, probably "some" effect on initial load, wich might be reduced with a compressed texture but probably low effect once it is loaded
- three.js dev extension allows to inspect the three.js scene graph, and set nodes to invisible: https://chrome.google.com/webstore/detail/threejs-developer-tools/ebpnegggocnnhleeicgljbedjkganaek
- for fps testing, a chrome shortcut with the args "--disable-frame-rate-limit --disable-gpu-vsync" is useful (too see possible improvement > 60 fps )
- Loads of triangles (> 400k), interesting to watch performance while disabling groups with the three.js extension
- maybe the cylinders of the data visialization can be improved, which seem to take a lot of computation time
    - Batching gemoetry sharing the same material (e.g. all blue cylinders) could improve the visualization
    - https://threejs.org/docs/index.html#examples/en/utils/BufferGeometryUtils.mergeBufferGeometries

#### Cal-Ver
I think I have now a good idea, how you could combine the advantages of GIT versioning and branches, while being able to deploy date-versioned copies to GH pages...

- My first idea was that each folder needs to be self-contained (no links/references to a folder outside of a single version), but this would cause high storage requirements for the binary assets (e.g. C19 would need 30MB per version => ~1 GB for 30 versions)
- If all different versions are sibling-folders, relative links to shared resources stay the same. But, when shared resources need to change, only new files/folders can be added, not deleted or modified.
- We can create a script which takes the current dev version, and copies it to a sibling-folder with the current branch name (e.g. v2020-04-18)
- additionally, everytime the current stable folder will be overwritten
- the result will be pushed to a gh_pages branch, wich will be configured to be used for Github pages

## 2020-04-16

- Performance Tips&Tricks: https://discoverthreejs.com/tips-and-tricks/#performance
- Mob-Programming commits from Harald's workshop session: https://github.com/approvals/ApprovalTests.cpp/commits/master
- Three.js Stats: https://github.com/mrdoob/stats.js/

### Follow-up from previous session

Cookbook sample demo [dmt-div-touch-move]( https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/dmt-div-move-touch/dmt-globe-basic-2020-04-11.html)

Source: https://github.com/ladybug-tools/spider-covid-19-viz-3d/blob/master/cookbook/dmt-div-move-touch/dmt-div-move-touch.js

## 2020-04-13

- interesting globe visualization libs
    - https://github.com/vasturiano/three-globe
    - https://github.com/vasturiano/globe.gl
    - https://github.com/vasturiano/globe-ar
- other interesting Covid-19 Visualizations:
    - https://covid19visualiser.com/
    - https://discourse.threejs.org/t/3d-covid-19-visualisation/13780
    - https://github.com/patricknasralla/covid19_3D_visualisation
    - https://codepen.io/Flamov/pen/MozgXb?editors=0010
- Theo's efforts with Vasturiano's wonderful code
	- https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/cookbook-gallery-2020-04-07.html#parse-geojson/parse-geojson-2020-03-17-00.html
	- https://www.ladybug.tools/spider-covid-19-viz-3d/cookbook/cookbook-gallery-2020-04-07.html#parse-geojson/parse-geojson-2020-03-17-12-05.html

_2020-04-12 Theo: I did not continue with Vasturiano's efforts because becoming familiar with the code and workow would have taken me many days. In particular I was looking for mouseover and mouse click interaction with elements, but could find no straightforward way of doing so. On the other hand, this could be a no-brainer for Harald's pair programming pairing visualizations._

### accomplishments

* https://www.ladybug.tools/spider-covid-19-viz-3d/dev/v-2020-04-12/covid-19-remix/c19-remix.html
	* Beginning to look good
* Theo's ESLint: all of Mr.doob's rules in use
	* Where the options listed for rules like this
	* https://eslint.org/docs/rules/no-undef is unclear

```
    /* Variables */
    "no-undef": 1,
    //"no-unused-vars": 1,
```

### Follow-up

### The Four Rules of Improv

_Theo: I try to keep these rules in mind when I communicate with people_

	Rule 1: Say Yes. The first rule of improvisation is AGREE. Always agree and SAY YES. ...
	Rule 2: Say Yes AND. The second rule of improvisation is not only to say yes, but YES, AND. ...
	Rule 3: Make Statements. The next rule is MAKE STATEMENTS. ...
	Rule 4: There Are No Mistakes. THERE ARE NO MISTAKES, only opportunities.

* [4 Rules of Improv and How They Relate to Customer Support]( https://zapier.com/learn/customer-support/improv-customer-support/ )
* [Tina Fey Schools Google's Eric Schmidt on Improv Comedy]( https://www.theatlantic.com/technology/archive/2011/04/tina-fey-schools-googles-eric-schmidt-comedy-improv/350037/ )



## 2020-04-08

- Links for the curve visualization:
    - Bezier Curve: https://threejs.org/docs/#api/en/extras/curves/QuadraticBezierCurve3
    - Raycaster: https://threejs.org/docs/#api/en/core/Raycaster
    - Line Material: https://threejs.org/examples/#webgl_lines_colors
    - Some Article: https://medium.com/@xiaoyangzhao/drawing-curves-on-webgl-globe-using-three-js-and-d3-draft-7e782ffd7ab

Harald's Blog Post about Week 1 of Pair-Programming Tour: https://dev.to/harald3dcv/remote-mob-programming-hunter-industries-18ac

Updates since Monday:

### [Cookbook]( https://ladybug-tools.github.io/spider-covid-19-viz-3d/cookbook )

* Demos of the various modules used to build the viewer

### [Archive Gallery 2]( https://ladybug-tools.github.io/spider-covid-19-viz-3d/dev/covid-19-viz-3d-archive )

* Files for every daily project update since 200-03-19. Some files are better than others.

### [Archive Gallery 1]( https://ladybug-tools.github.io/spider-covid-19-viz-3d/dev/covid-19-statistics-on-globe )

* Files from the first day of the project

No work on ESLint yet



## 2020-04-06

* Move this repo to an organization?
- Adrian Bolboaca - Remote Pair-Programming: https://www.youtube.com/channel/UC7H7P2tu2i3Wnz-ZBdnO13Q

- Mr.Doob Approves Rules: https://github.com/zz85/mrdoobapproves/blob/gh-pages/mdcs_eslint.js


## 2020-04-02

> If it hurts, do it more often.
>
> *-- Jez Humble* from https://continuousdelivery.com/

- https://jsonbox.io/
- force ESLint formatter: https://github.com/microsoft/vscode-eslint/issues/417#issuecomment-566209216

## 2020-03-31

Links to Harald's "Google Summer of Code" proposals:
- Blender: https://docs.google.com/document/d/1jhj93fujGBkRm0z1FdE2_MOWuOOgKerYJdjSEYEooRA/edit?usp=sharing
- Inkskape: https://docs.google.com/document/d/1Jf-NruuNOqh-_lIeqLbIvhRfjJs3BmuFamjDeGeu-rA/edit?usp=sharing

Interesting (Synced) Mob-Programming timer (could be also used for Pair-Programming):
- https://github.com/mrozbarry/mobtime

## 2020-03-30 - Evening Session

### DateVer/CalVer (instead of SemVer)

- https://calver.org/



## 2020-03-30 - Morning Session

### Mostly Harald

Let's get started with pair-programming.

For the beginning we decided to loosely start pair-programming with the traditional Driver/Navigator roles, and switch roles after several minutes using a timer.


Here are some pair-programming resources:

- https://www.thekguy.com/2-tips-to-make-pair-programming-less-scary.html
- https://medium.com/@volkanbier_42259/how-to-put-pair-programming-into-action-ce9ebb9d711
- https://en.wikipedia.org/wiki/Pair_programming
- https://www.agilealliance.org/glossary/pairing/
- https://martinfowler.com/articles/on-pair-programming.html
- https://stackify.com/pair-programming-advantages/
- https://hackernoon.com/the-ultimate-guide-to-pair-programming-b606625bc784
- https://wiki.c2.com/?PairProgramming


Here are timers we can use for starting pairing:

- https://double-trouble.wielo.co/
- https://theo-armour.github.io/qdata/apps/dashboard3/teodori-2020-02-08.html


Initial configuration of eslint we created during our first session:

- https://github.com/haraldreingruber/spider-covid-19-viz-3d/tree/feature/linting

### Mostly Theo


#### Node

I had uninstalled node, so I have to reinstall

* http://nodejs.org

Install suggested version: node-v12.16.1-x64.msi

Node installed in C:\Program Files\nodejs\

Chocolatey installs Python 2.7!!!!

#### NPM

* https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

In Git Bash

$ npm install npm -g

later

$ npm -v
6.14.4

#### ESLint

* https://eslint.org/docs/user-guide/getting-started

In Git Bash

$ npm install eslint --save-dev

	npm WARN deprecated mkdirp@0.5.4: Legacy versions of mkdirp are no longer supported. Please update to mkdirp 1.x. (Note that the API surface has changed to use Promises in 1.x.)
	npm WARN saveError ENOENT: no such file or directory, open 'C:\Users\tarmo\package.json'
	npm notice created a lockfile as package-lock.json. You should commit this file.
	npm WARN enoent ENOENT: no such file or directory, open 'C:\Users\tarmo\package.json'
	npm WARN tarmo No description
	npm WARN tarmo No repository field.
	npm WARN tarmo No README data
	npm WARN tarmo No license field.

	+ eslint@6.8.0
	added 134 packages from 84 contributors and audited 179 packages in 8.033s

	7 packages are looking for funding
	run `npm fund` for details

	found 0 vulnerabilities

$ npx eslint --init

Remember

- Very tricky. Install process is dumber than you think.
- You actually have to edit the lines to move the ">" symbol
- Once you have edited the line, go to to the end of line before pressing enter

$ npx eslint main.js

Takes over 7 seconds to run

Could install globally, but:

> It is also possible to install ESLint globally rather than locally (using npm install eslint --global). However,
> this is not recommended, and any plugins or shareable configs that you use must be installed locally in either case.

$ npx eslint main.js --fix

Does not seem to save changes

#### ESLint in VS Code

22:39  ~ Installed, but not working




